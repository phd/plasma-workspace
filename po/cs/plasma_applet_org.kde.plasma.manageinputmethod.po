# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2021-07-26 14:16+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.04.3\n"

#: contents/ui/manage-inputmethod.qml:64
#, kde-format
msgctxt "Opens the system settings module"
msgid "Configure Virtual Keyboards..."
msgstr "Nastavit virtuální klávesnice..."

#: contents/ui/manage-inputmethod.qml:79
#, kde-format
msgid "Virtual Keyboard: unavailable"
msgstr "Virtuální klávesnice: nedostupná"

#: contents/ui/manage-inputmethod.qml:90
#, kde-format
msgid "Virtual Keyboard: disabled"
msgstr "Virtuální klávesnice: zakázána"

#: contents/ui/manage-inputmethod.qml:104
#, kde-format
msgid "Show Virtual Keyboard"
msgstr ""

#: contents/ui/manage-inputmethod.qml:115
#, kde-format
msgid "Virtual Keyboard: visible"
msgstr "Virtuální klávesnice: viditelná"

#: contents/ui/manage-inputmethod.qml:128
#, kde-format
msgid "Virtual Keyboard: enabled"
msgstr "Virtuální klávesnice: povolena"
