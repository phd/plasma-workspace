# Language  translations for KDE package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the KDE package.
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: KDE 5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-02 00:47+0000\n"
"PO-Revision-Date: 2021-12-29 18:03+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.07.70\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: webshortcutrunner.cpp:58
#, kde-format
msgid "Opens \"%1\" in a web browser with the query :q:."
msgstr "يفتح \"%1\" في متصفّح الوِب بالاستعلام :q:."

#: webshortcutrunner.cpp:63
#, kde-format
msgid "Search using the DuckDuckGo bang syntax"
msgstr "ابحث باستعمال تراكيب DuckDuckGo"

#: webshortcutrunner.cpp:94
#, kde-format
msgid "Search in private window"
msgstr "ابحث في نافذة خاصة"

#: webshortcutrunner.cpp:94
#, kde-format
msgid "Search in incognito window"
msgstr "ابحث في نافذة تصفح خفي"

#: webshortcutrunner.cpp:129 webshortcutrunner.cpp:146
#, kde-format
msgid "Search %1 for %2"
msgstr "ابحث %1 عن %2"
