# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015, 2016.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:48+0000\n"
"PO-Revision-Date: 2022-05-22 07:02+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 21.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrej Mernik,Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrejm@ubuntu.si,matjaz.jeran@amis.net"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Prazniki"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Dogodki"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Za narediti"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Ostalo"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Zaklep zaslona omogočen"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Nastavi ali bo zaslon po navedenem času zaklenjen."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Časovni pretek ohranjevalnika zaslona"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Nastavitev števila minut, ki pretečejo preden se zaslon zaklene."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nova seja"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtri"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:42
#, kde-format
msgid "Select Plasmoid File"
msgstr "Izberi datoteko s Plasmoidom"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Nameščanje paketa %1 ni uspelo."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installation Failure"
msgstr "Spodletela namestitev"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:110
#, kde-format
msgid "All Widgets"
msgstr "Vsi gradniki"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
#, kde-format
msgid "Running"
msgstr "Teče"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Neodstranljiv"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
#, kde-format
msgid "Categories:"
msgstr "Kategorije:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:199
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Prejmi nove gradnike Plasme"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:208
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Namesti gradnik iz krajevne datoteke…"

#~ msgid "&Execute"
#~ msgstr "Izv&edi"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Predloge"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Konzola za skriptanje za namizno lupino"

#~ msgid "Editor"
#~ msgstr "Urejevalnik"

#~ msgid "Load"
#~ msgstr "Naloži"

#~ msgid "Use"
#~ msgstr "Uporabi"

#~ msgid "Output"
#~ msgstr "Izhod"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Ni mogoče naložiti skriptne datoteke <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Odpri skriptno datoteko"

#~ msgid "Save Script File"
#~ msgstr "Shrani skriptno datoteko"

#~ msgid "Executing script at %1"
#~ msgstr "Izvajanje skripta v %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Izvajalni čas: %1 ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Prenesi vtičnike ozadja"

#~ msgid "Containments"
#~ msgstr "Vsebniki"

#~ msgctxt ""
#~ "%1 is a type of widgets, as defined by e.g. some plasma-packagestructure-"
#~ "*.desktop files"
#~ msgid "Download New %1"
#~ msgstr "Prejmi novo: %1"
