# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-05 00:46+0000\n"
"PO-Revision-Date: 2022-08-24 15:19+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "Opna umlykjandi möppu"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audio"
msgstr "Hljóð"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Image"
msgstr "Mynd"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Video"
msgstr "Myndskeið"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheet"
msgstr "Töflureiknisskjal"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentation"
msgstr "Kynning"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folder"
msgstr "Mappa"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Document"
msgstr "Skjal"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archive"
msgstr "Safnskrá"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Text"
msgstr "Texti"
